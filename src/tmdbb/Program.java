package tmdbb;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;

import info.movito.themoviedbapi.model.core.SessionToken;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Tanner
 * The start point for the application.
 */
public class Program extends Application {
	private static SessionToken sessionToken;
	
	/**
	 * The api key for this application.
	 */
	public static final String API_KEY = "0446b9f1c98492eff703dfd1ed5b51c6";

	@Override
    public final void start(final Stage primaryStage) throws Exception {
		Parent parent = FXMLLoader.load(getClass().getResource(
		        "view/MainWindowView.fxml"));
		Scene scene = new Scene(parent);
		scene.getStylesheets().add(getClass().getResource(
		        "view/stylesheets/Light.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	
	/**
	 * @param token The session token to be saved
	 */
	public static void setSessionToken(final SessionToken token) {
		sessionToken = token;
		PrintWriter writer;
		try {
			writer = new PrintWriter("sessionToken.txt", "UTF-8");
			writer.println(sessionToken == null ? "" : sessionToken.toString());
			writer.close();
		} catch (FileNotFoundException e) {
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return Gets the sessionToken for the user logged in currently
	 */
	public static SessionToken getSessionToken() {
		if (sessionToken == null) {
			try {
				List<String> lines = Files.readAllLines(FileSystems.getDefault()
				        .getPath("sessionToken.txt"));
				if (!lines.isEmpty()) {
					String sessionTokenStr = lines.get(0);
					if (sessionTokenStr != null 
					        && !sessionTokenStr.equals("")) {
                        sessionToken = new SessionToken(sessionTokenStr);
                    }
				}
			} catch (IOException e) {
			}
		}
		return sessionToken;
	}
}
