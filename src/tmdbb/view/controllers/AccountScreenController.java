package tmdbb.view.controllers;

import java.awt.event.ActionListener;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.AccountID;
import info.movito.themoviedbapi.model.core.MovieResultsPage;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import tmdbb.Program;
import tmdbb.view.controls.MoviesPanel;
import tmdbb.view.controls.ScreensPanel;

/**
 * The Account Screen Controller
 * 
 * @author Tanner
 *
 */
public class AccountScreenController extends ScreenController
		implements Initializable {
	@FXML
	private AnchorPane userListsPane;

	private Button moreButton;
	private int page = 0;
	private boolean isMore = false;
	@FXML
	private MoviesPanel movies;

	@FXML
	private VBox spinner;

	@FXML
	private Text status;

	@FXML
	private Label welcome;

	@FXML
	private ScreensPanel leftScreens;

	private LoginScreenController loginController;

	@FXML
	private Text text;
	@FXML
	private Button btnLogout;

	private void setBusy(boolean busy) {
		if (busy) {
			spinner.setVisible(true);
			if (movies != null) {
				movies.getChildren().forEach(x -> {
					x.setDisable(true);
				});
			}
		} else {
			spinner.setVisible(false);
			if (movies != null) {
				movies.getChildren().forEach(x -> {
					x.setDisable(false);
				});
			}
		}
	}

	@FXML
	private void onLogout() {
		Program.setSessionToken(null);
		leftScreens.setScreen("login");
		movies.clear();
		welcome.setText("");
		userListsPane.setDisable(true);
	}

	@FXML
	private void onWatchlist(final ActionEvent event) {
		page = 1;
		getWatchlistAsync(page);
		status.setText("Displaying: Watchlist");
	}

	@FXML
	private void onFavorites(final ActionEvent event) {
		page = 1;
		getFavoritesAsync(page);
		status.setText("Displaying: Favorites");
	}

	private synchronized boolean getIsMore() {
		return isMore;
	}

	private synchronized void setIsMore(boolean pIsMore) {
		isMore = pIsMore;
	}

	private void getWatchlistAsync(int pageNum) {
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				List<MovieDb> np = getWatchlist(pageNum);
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						if (pageNum == 1) {
							movies.setItems(np, false);
						} else {
							movies.addAll(np, false);
						}

						movies.remove(moreButton);

						if (getIsMore()) {
							moreButton = new Button("More");
							moreButton.addEventFilter(ActionEvent.ANY, (x -> {
								getWatchlistAsync(++page);
							}));
							movies.add(moreButton);
						}

						setBusy(false);
					}
				});

				return null;
			}
		};
		Thread t = new Thread(task);
		t.start();
	}

	private void getFavoritesAsync(int pageNum) {
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				List<MovieDb> np = getFavorites(pageNum);
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						if (pageNum == 1) {
							movies.setItems(np, false);
						} else {
							movies.addAll(np, false);
						}

						movies.remove(moreButton);

						if (getIsMore()) {
							moreButton = new Button("More");
							moreButton.addEventFilter(ActionEvent.ANY, (x -> {
								getFavoritesAsync(++page);
							}));
							movies.add(moreButton);
						}

						setBusy(false);

					}
				});

				return null;
			}
		};
		Thread t = new Thread(task);
		t.start();
	}

	public final List<MovieDb> getFavorites(final int pageNum) {
		TmdbApi api = new TmdbApi(Program.API_KEY);
		AccountID id = new AccountID(
				api.getAccount().getAccount(Program.getSessionToken()).getId());
		MovieResultsPage ppage = api.getAccount()
				.getFavoriteMovies(Program.getSessionToken(), id);
		setIsMore(ppage.getTotalPages() > pageNum);
		return ppage.getResults();
	}

	public final List<MovieDb> getWatchlist(final int pageNum) {
		TmdbApi api = new TmdbApi(Program.API_KEY);
		AccountID id = new AccountID(
				api.getAccount().getAccount(Program.getSessionToken()).getId());
		MovieResultsPage ppage = api.getAccount()
				.getWatchListMovies(Program.getSessionToken(), id, pageNum);
		setIsMore(ppage.getTotalPages() > pageNum);
		return ppage.getResults();
	}

	@Override
	public final void initialize(final URL location,
			final ResourceBundle resources) {

		loginController = (LoginScreenController) leftScreens
				.loadScreen("login", "../LoginScreen.fxml");
		loginController.addLoggedInListener(new ActionListener() {

			@Override
			public void actionPerformed(final java.awt.event.ActionEvent e) {
				leftScreens.setScreen(null);
				TmdbApi api = new TmdbApi(Program.API_KEY);
				String userName = api.getAccount()
						.getAccount(Program.getSessionToken()).getUserName();
				welcome.setText("Welcome, " + userName);
				userListsPane.setDisable(false);
			}
		});
		if (Program.getSessionToken() == null) {
			leftScreens.setScreen("login");
			userListsPane.setDisable(true);
		} else {
			userListsPane.setDisable(false);
			Task<Void> task = new Task<Void>() {
				protected Void call() {
					TmdbApi api = new TmdbApi(Program.API_KEY);
					final String userName = api.getAccount()
							.getAccount(Program.getSessionToken())
							.getUserName();
					Platform.runLater(() -> {
						welcome.setText("Welcome, " + userName);
					});
					return null;
				}
			};
			new Thread(task).start();

		}

	}

}
