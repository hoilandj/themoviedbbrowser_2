package tmdbb.view.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import tmdbb.Program;
import tmdbb.view.controls.MoviesPanel;

/**
 * The Browser screen controller, this is the main screen.
 * 
 * @author Tanner
 *
 */
public class BrowserScreenController extends ScreenController
		implements Initializable {

	private Button moreButton;
	private int page = 0;
	private boolean isMore = false;
	@FXML
	private MoviesPanel movies;

	@FXML
	private Text status;

	@FXML
	private TextField keyword;

	@FXML
	private VBox spinner;

	private String lastKeyword;

	private void setBusy(boolean busy) {
		if (busy) {
			spinner.setVisible(true);
			if (movies != null) {
				movies.getChildren().forEach(x -> {
					x.setDisable(true);
				});
			}
		} else {
			spinner.setVisible(false);
			if (movies != null) {
				movies.getChildren().forEach(x -> {
					x.setDisable(false);
				});
			}
		}
	}

	@Override
	public final void initialize(final URL location,
			final ResourceBundle resources) {
	}

	@FXML
	private void onSearch(final ActionEvent event) {
		if (keyword.getText() != null && !keyword.getText().equals("")) {
			page = 1;
			lastKeyword = keyword.getText();
			searchAsync(lastKeyword, page);
			status.setText(String.format("Displaying: Search Results (%s)",
					lastKeyword));
		}
	}

	@FXML
	private void onNowPlaying(final ActionEvent event) {
		page = 1;
		getNowPlayingAsync(page);
		status.setText("Displaying: Now Playing");
	}

	@FXML
	private void onTopRated(final ActionEvent event) {
		page = 1;
		getTopRatedAsync(page);
		status.setText("Displaying: Top Rated");
	}

	private void searchAsync(final String keyword, final int pageNum) {
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				List<MovieDb> np = searchMovies(keyword, pageNum);
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						if (pageNum == 1) {
							movies.setItems(np,
									Program.getSessionToken() != null);
						} else {
							movies.addAll(np,
									Program.getSessionToken() != null);
						}

						movies.remove(moreButton);

						if (getIsMore()) {
							moreButton = new Button("More");
							moreButton.addEventFilter(ActionEvent.ANY, (x -> {

								searchAsync(lastKeyword, ++page);

							}));
							movies.add(moreButton);
						}

						setBusy(false);
					}
				});

				return null;
			}
		};
		Thread t = new Thread(task);
		t.start();
	}

	private void getNowPlayingAsync(int pageNum) {
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				List<MovieDb> np = getNowPlaying(pageNum);
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						if (pageNum == 1) {
							movies.setItems(np,
									Program.getSessionToken() != null);
						} else {
							movies.addAll(np,
									Program.getSessionToken() != null);
						}

						movies.remove(moreButton);

						if (getIsMore()) {
							moreButton = new Button("More");
							moreButton.addEventFilter(ActionEvent.ANY, (x -> {
								getNowPlayingAsync(++page);
							}));
							movies.add(moreButton);
						}

						setBusy(false);
					}
				});

				return null;
			}
		};
		Thread t = new Thread(task);
		t.start();
	}

	private void getTopRatedAsync(int pageNum) {
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				List<MovieDb> np = getTopRated(pageNum);
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						if (pageNum == 1) {
							movies.setItems(np,
									Program.getSessionToken() != null);
						} else {
							movies.addAll(np,
									Program.getSessionToken() != null);
						}

						movies.remove(moreButton);

						if (getIsMore()) {
							moreButton = new Button("More");
							moreButton.addEventFilter(ActionEvent.ANY, (x -> {
								getTopRatedAsync(++page);
							}));
							movies.add(moreButton);
						}

						setBusy(false);

					}
				});

				return null;
			}
		};
		Thread t = new Thread(task);
		t.start();
	}

	private synchronized boolean getIsMore() {
		return isMore;
	}

	private synchronized void setIsMore(boolean pIsMore) {
		isMore = pIsMore;
	}

	/**
	 * @param keyword
	 *            The key word to be searched
	 * @param pageNum
	 *            the page number to display
	 * @return a list of the movies
	 */
	public final List<MovieDb> searchMovies(final String keyword,
			final int pageNum) {
		MovieResultsPage ppage = new TmdbApi("0446b9f1c98492eff703dfd1ed5b51c6")
				.getSearch().searchMovie(keyword, 0, "en", true, pageNum);
		setIsMore(ppage.getTotalPages() > pageNum);
		return ppage.getResults();
	}

	/**
	 * @param pageNum
	 *            the page number to display
	 * @return a list of movies from result
	 */
	public final List<MovieDb> getNowPlaying(final int pageNum) {
		MovieResultsPage ppage = new TmdbApi("0446b9f1c98492eff703dfd1ed5b51c6")
				.getMovies().getNowPlayingMovies("en", pageNum);
		setIsMore(ppage.getTotalPages() > pageNum);
		return ppage.getResults();
	}

	/**
	 * @param pageNum
	 *            the page number to display
	 * @return the list of movies from result
	 */
	public final List<MovieDb> getTopRated(final int pageNum) {
		MovieResultsPage ppage = new TmdbApi("0446b9f1c98492eff703dfd1ed5b51c6")
				.getMovies().getTopRatedMovies("en", pageNum);
		setIsMore(ppage.getTotalPages() > pageNum);
		return ppage.getResults();
	}
}
