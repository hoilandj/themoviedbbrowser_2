package tmdbb.view.controllers;

/**
 * The model for all controllers to extend
 * @author Tanner
 *
 */
public abstract class Controller {

	/**
	 * The model to be used by this controller.
	 */
	protected Object model;

	/**
	 * @param pModel the new models
	 */
	public void setModel(final Object pModel) {
		model = pModel;
	}

	/**
	 * @return the model currently stored
	 */
	public final Object getModel() {
		return model;
	}

}
