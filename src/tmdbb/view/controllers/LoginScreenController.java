package tmdbb.view.controllers;

import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.TmdbAuthentication;
import info.movito.themoviedbapi.model.config.TokenAuthorisation;
import info.movito.themoviedbapi.model.core.SessionToken;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import tmdbb.Program;

/**
 * @author Tanner
 *
 */
public class LoginScreenController extends ScreenController {
	private List<ActionListener> loggedinListeners = new ArrayList<>();

	@FXML
	private VBox spinner;

	@FXML
	private Button btnWebRegister;

	@FXML
	private AnchorPane root;

	@FXML
	private Text status;

	@FXML
	private PasswordField password;

	@FXML
	private TextField userName;

	@FXML
	private void onKeyPressed(final KeyEvent event) {
		if (event.getCode().equals(KeyCode.ENTER)) {
			onLogin();
		}
	}

	@FXML
	private void onCancel(final javafx.event.ActionEvent event) {
		screensPanel.setScreen("browser");
	}

	public final void addLoggedInListener(final ActionListener listener) {
		loggedinListeners.add(listener);
	}

	public final void removeLoggedInListener(final ActionListener event) {
		loggedinListeners.remove(event);
	}

	private void raiseLoggedIn() {
		for (ActionListener l : loggedinListeners) {
			l.actionPerformed(null);
		}
	}

	private void setBusy(boolean busy) {
		if (busy) {
			spinner.setVisible(true);
			if (root != null) {
				root.setDisable(true);
			}
		} else {
			spinner.setVisible(false);
			if (root != null) {
				root.setDisable(false);
			}
		}
	}

	@FXML
	private void onWebBrowser(final javafx.event.ActionEvent event) {
		btnWebRegister.setDisable(true);
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			protected Void call() {
				Boolean success = false;
				try {
					TmdbAuthentication tmdbAuth = new TmdbApi(Program.API_KEY)
							.getAuthentication();
					TokenAuthorisation requestToken = tmdbAuth
							.getAuthorisationToken();
					Desktop.getDesktop()
							.browse(new URI(
									"https://www.themoviedb.org/authenticate/"
											+ requestToken.getRequestToken()));

					int tries = 0;
					int maxTries = 10;
					int sleepTime = 5000;
					while (!success && tries < maxTries) {
						try {
							++tries;
							Program.setSessionToken(new SessionToken(
									tmdbAuth.getSessionToken(requestToken)
											.getSessionId()));
							success = true;
						} catch (Exception ex) {
							Thread.sleep(sleepTime);
						}
					}

				} catch (Exception ex) {
				}

				if (success) {
					Platform.runLater(() -> {
						btnWebRegister.setDisable(false);
						raiseLoggedIn();
						setBusy(false);
					});

				} else {
					Platform.runLater(() -> {
						setBusy(false);
						status.setText("Login Failed.");
						btnWebRegister.setDisable(false);
					});
				}

				return null;
			}
		};
		Thread t = new Thread(task);
		t.start();

	}

	@FXML
	private void onLogin() {

		if (userName.getText().equals("") || password.getText().equals("")) {
			status.setText("User name and password are required.");
			return;
		}
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			protected Void call() {

				Boolean success = true;

				try {
					Program.setSessionToken(new SessionToken(
							new TmdbApi(Program.API_KEY).getAuthentication()
									.getSessionLogin(userName.getText(),
											password.getText())
									.getSessionId()));
				} catch (Exception ex) {
					success = false;
				}

				if (success) {
					Platform.runLater(() -> {
						raiseLoggedIn();
						setBusy(false);
					});

				} else {
					Platform.runLater(() -> {
						status.setText("Login Failed.");
						setBusy(false);
					});

				}

				return null;
			}
		};
		new Thread(task).start();

	}
}
