package tmdbb.view.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import tmdbb.view.controls.ScreensPanel;

/**
 * The main window controller.
 * 
 * @author Tanner
 *
 */
public class MainWindowViewController extends Controller
		implements Initializable {

	@FXML
	private Button btnAccount;
	@FXML
	private Button btnBrowse;

	@FXML
	private ScreensPanel screens;

	@FXML
	private void onQuit(final ActionEvent event) {
		Platform.exit();
	}

	@FXML
	private void btnAccount_onAction(final ActionEvent event) {
		screens.setScreen("account");
	}

	@FXML
	private void btnBrowse_onAction() {
		screens.setScreen("browser");
	}

	@Override
	public final void initialize(final URL location,
			final ResourceBundle resources) {
		screens.loadScreen("account", "../AccountScreen.fxml");
		screens.loadScreen("browser", "../BrowserScreen.fxml");

		screens.setScreen("browser");
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				btnBrowse.requestFocus();
			}
		});
	}

}
