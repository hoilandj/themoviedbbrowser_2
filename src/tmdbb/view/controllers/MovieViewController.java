package tmdbb.view.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import info.movito.themoviedbapi.TmdbAccount.MediaType;
import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.Utils;
import info.movito.themoviedbapi.TmdbMovies.MovieMethod;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.ReleaseInfo;
import info.movito.themoviedbapi.model.core.AccountID;
import info.movito.themoviedbapi.model.core.ResponseStatus;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import tmdbb.Program;

/**
 * The individual Movie view controller
 * 
 * @author Tanner
 *
 */
public class MovieViewController extends Controller implements Initializable {

	@FXML
	Label avgRating;

	@FXML
	Button addToWatchlist;

	@FXML
	Button addToFavorites;

	@FXML
	private VBox spinner;

	@FXML
	private HBox movieInfo;

	@FXML
	private ImageView image;

	@FXML
	private Label title;

	@FXML
	private Label runtime;

	@FXML
	private Label year;

	@FXML
	private Label mpaa;

	@FXML
	private TextArea synopsis;

	private MovieDb movie;

	private void setBusy(final boolean busy) {
		if (busy) {
			spinner.setVisible(true);
			if (movieInfo != null) {
				movieInfo.getChildren().forEach(x -> {
					x.setDisable(true);
				});
			}
		} else {
			spinner.setVisible(false);
			if (movieInfo != null) {
				movieInfo.getChildren().forEach(x -> {
					x.setDisable(false);
				});
			}
		}
	}

	@Override
	public void setModel(Object pModel) {
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			protected Void call() {

				model = movie = (new TmdbApi(Program.API_KEY).getMovies()
						.getMovie(movie.getId(), "en", MovieMethod.releases,
								MovieMethod.credits));

				if (movie.getPosterPath() != null) {
					Image i = new Image(
							Utils.createImageUrl(new TmdbApi(Program.API_KEY),
									movie.getPosterPath(), "w92").toString());
					Platform.runLater(() -> {
						image.setImage(i);
					});
				} else {
					Image i = new Image("notFound.png");

					Platform.runLater(() -> {
						image.setImage(i);
					});
				}

				if (movie.getReleases() != null
						&& !movie.getReleases().isEmpty()) {
					String mpaaCertification = "";
					for (ReleaseInfo ri : movie.getReleases()) {
						if (ri.getCountry().equals("US")) {
							mpaaCertification = ri.getCertification();
							break;
						}
					}
					final String s = mpaaCertification;
					Platform.runLater(() -> {
						mpaa.setText(s);
					});
				}

				Platform.runLater(() -> {
					setBusy(false);
				});
				return null;
			}
		};
		Thread t = new Thread(task);
		t.start();

		movie = (MovieDb) pModel;
		if (movie.getTitle() != null) {
			title.setText(movie.getTitle());
		}
		if (movie.getOverview() != null) {
			synopsis.setText(movie.getOverview());
		}
		if (movie.getRuntime() > 0) {
			runtime.setText("(" + movie.getRuntime() + " mins)");
		}
		if (movie.getReleaseDate() != null) {
			year.setText(movie.getReleaseDate().split("-")[0]);
		}
		avgRating.setText(new Float(movie.getVoteAverage()).toString() + "/10");
	}

	@FXML
	private void onAddToWatchlist(ActionEvent event) {
		if (Program.getSessionToken() == null) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Login Required");
			alert.setHeaderText(null);
			alert.setContentText(
					"You must be logged in to add to your watchlist.");
			alert.showAndWait();
			return;
		}
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			protected Void call() {

				TmdbApi api = new TmdbApi(Program.API_KEY);
				int aId = api.getAccount().getAccount(Program.getSessionToken())
						.getId();
				AccountID id = new AccountID(aId);
				ResponseStatus response = api.getAccount().addToWatchList(
						Program.getSessionToken(), id, movie.getId(),
						MediaType.MOVIE);

				Platform.runLater(() -> {
					setBusy(false);
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Add To Watchlist");
					alert.setHeaderText(null);
					alert.setContentText(response.getStatusMessage());
					alert.showAndWait();
				});
				return null;
			}
		};
		new Thread(task).start();
	}

	@FXML
	private void onAddToFavorites(ActionEvent event) {
		if (Program.getSessionToken() == null) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Login Required");
			alert.setHeaderText(null);
			alert.setContentText(
					"You must be logged in to add to your favorites.");
			alert.showAndWait();
			return;
		}
		setBusy(true);
		Task<Void> task = new Task<Void>() {
			protected Void call() {

				TmdbApi api = new TmdbApi(Program.API_KEY);
				int aId = api.getAccount().getAccount(Program.getSessionToken())
						.getId();
				AccountID id = new AccountID(aId);
				ResponseStatus response = api.getAccount().addFavorite(
						Program.getSessionToken(), id, movie.getId(),
						MediaType.MOVIE);

				Platform.runLater(() -> {
					setBusy(false);
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Add To Favorites");
					alert.setHeaderText(null);
					alert.setContentText(response.getStatusMessage());
					alert.showAndWait();
				});
				return null;
			}
		};
		new Thread(task).start();
	}

	@Override
	public final void initialize(final URL location,
			final ResourceBundle resources) {
	}

	public void showUserButtons(boolean show) {
		addToFavorites.setVisible(show);
		addToFavorites.setManaged(show);
		addToWatchlist.setManaged(show);
		addToWatchlist.setVisible(show);

	}

}
