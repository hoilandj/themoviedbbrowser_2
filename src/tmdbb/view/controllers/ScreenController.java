package tmdbb.view.controllers;

import tmdbb.view.controls.ScreensPanel;

public abstract class ScreenController {
	protected ScreensPanel screensPanel;

	public void setParentScreensPanel(ScreensPanel pScreensPanel) {
		screensPanel = pScreensPanel;
	}

}
