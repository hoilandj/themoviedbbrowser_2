package tmdbb.view.controls;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import info.movito.themoviedbapi.model.MovieDb;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import tmdbb.view.controllers.Controller;
import tmdbb.view.controllers.MovieViewController;

/**
 * @author Tanner The panel that displays the movies
 */
public class MoviesPanel extends AnchorPane {
	private Hashtable<Node, Controller> ht = new Hashtable<>();
	private AnchorPane credits;
	private ListView<Node> listView;
	private GridPane grid;

	/**
	 * Constructor. Calls init and super
	 */
	public MoviesPanel() {
		super();
		init();
	}

	private void createListView() {
		if (grid != null && listView != null
				&& grid.getChildren().contains(listView)) {
			grid.getChildren().remove(listView);
		}
		listView = new ListView<Node>();
		listView.setOnMouseClicked(event -> {

			createCreditsPanel(listView.getSelectionModel().getSelectedItem());
		});
		GridPane.setColumnIndex(listView, 0);
		GridPane.setVgrow(listView, Priority.ALWAYS);
		GridPane.setHgrow(listView, Priority.ALWAYS);
	}

	private void createCreditsPanel(Node movieView) {
		if (movieView == null)
			return;
		grid.getChildren().remove(credits);
		MovieDb movie = (MovieDb) ht.get(movieView).getModel();
		credits = new AnchorPane();
		GridPane.setColumnIndex(credits, 1);
		GridPane.setVgrow(credits, Priority.ALWAYS);
		grid.getChildren().add(credits);

		VBox vbox = new VBox();
		AnchorPane.setLeftAnchor(vbox, 0.0);
		AnchorPane.setRightAnchor(vbox, 0.0);
		AnchorPane.setTopAnchor(vbox, 0.0);
		AnchorPane.setBottomAnchor(vbox, 0.0);

		ListView<String> creditsList = new ListView<>();
		if (movie.getCredits() != null) {
			if (movie.getCredits() != null) {
				creditsList.getItems().add("***** Cast *****");
				movie.getCredits().getCast().forEach(x -> {
					creditsList.getItems()
							.add(x.getName() + ": " + x.getCharacter());
				});
			}
			if (movie.getCredits().getCrew() != null) {
				creditsList.getItems().add("***** Crew *****");
				movie.getCredits().getCrew().forEach(x -> {
					creditsList.getItems().add(x.getName() + ": " + x.getJob());
				});
			}
		}
		VBox.setVgrow(creditsList, Priority.ALWAYS);

		HBox hbox = new HBox();

		Label label = new Label("Credits");
		Button x = new Button("x");
		x.addEventFilter(ActionEvent.ANY, (ev -> {
			grid.getChildren().remove(credits);

		}));
		hbox.getChildren().add(x);
		hbox.getChildren().add(label);
		vbox.getChildren().add(hbox);
		vbox.getChildren().add(creditsList);
		credits.getChildren().add(vbox);
	}

	private void init() {
		setPadding(new Insets(3.0, 14.0, 3.0, 14.0));

		grid = new GridPane();
		getChildren().add(grid);
		setLeftAnchor(grid, 0.0);
		setRightAnchor(grid, 0.0);
		setTopAnchor(grid, 0.0);
		setBottomAnchor(grid, 0.0);

		createListView();
	}

	/**
	 * clears the screen.
	 */
	public final void clear() {
		createListView();
		grid.getChildren().remove(credits);
		ht.clear();
	}

	/**
	 * removes the node from the panel.
	 * 
	 * @param node
	 *            the node to be removed
	 */
	public final void remove(final Node node) {
		listView.getItems().remove(node);
	}

	/**
	 * Adds the givin node to the screen.
	 * 
	 * @param node
	 *            the node being added
	 */
	public final void add(final Node node) {
		listView.getItems().add(node);
	}

	/**
	 * Adds the given movie to the screen.
	 * 
	 * @param pMovie
	 *            creates a new node and adds it to the screen
	 */
	public final void add(final MovieDb pMovie, boolean showUserButtons) {
		try {
			FXMLLoader myLoader = new FXMLLoader(
					getClass().getResource("../MovieView.fxml"));
			Node movieView = (Node) myLoader.load();
			Controller controller = (Controller) myLoader.getController();
			controller.setModel(pMovie);
			((MovieViewController) controller).showUserButtons(showUserButtons);
			listView.getItems().add(movieView);
			ht.put(movieView, controller);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param pMovies
	 *            adds all the movies passed to the screen
	 */
	public final void addAll(final List<MovieDb> pMovies,
			boolean showUserButtons) {
		for (MovieDb movie : pMovies) {
			add(movie, showUserButtons);
		}
	}

	/**
	 * @param pMovies
	 *            the movies to be displayed on the screen
	 */
	public final void setItems(final List<MovieDb> pMovies,
			boolean showUserButtons) {
		clear();
		addAll(pMovies, showUserButtons);
		if (!grid.getChildren().contains(listView)) {
			grid.getChildren().add(listView);
		}

	}
}
