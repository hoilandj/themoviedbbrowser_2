package tmdbb.view.controls;

import java.util.HashMap;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import tmdbb.view.controllers.ScreenController;

/**
 * @author Tanner
 * Custom panel type to handle loading and unloading multiple nodes.
 */
public class ScreensPanel extends AnchorPane {

	private HashMap<String, Node> screens = new HashMap<>();

	/**
	 * Constructor.
	 */
	public ScreensPanel() {
		super();
		setBackground(new Background(new BackgroundFill(Color.web("#" + "3333"),
		        CornerRadii.EMPTY, Insets.EMPTY)));

	}

	/**
	 * @param name arbatray name of the node
	 * @param resource the location of the resource
	 * Loads the fxml file, add the screen to the screens collection and
	 * finally injects the screenPane to the controller.
	 * @return and instance of the controller for the loaded view
	 */
	public final ScreenController loadScreen(final String name,
	        final String resource) {
		try {
			FXMLLoader myLoader = new FXMLLoader(getClass()
			        .getResource(resource));
			Parent loadScreen = (Parent) myLoader.load();
			ScreenController myScreenControler = ((ScreenController) 
			        myLoader.getController());
			myScreenControler.setParentScreensPanel(this);
			screens.put(name, loadScreen);
			return myScreenControler;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			return null;
		}
	}

	/**
	 * @param name the given name of the screen
	 * @return true if the screen gets set correctly
	 */
	public final boolean setScreen(final String name) {
		final DoubleProperty opacity = opacityProperty();
		final int duration = 350;
		if (name == null) {

			if (!getChildren().isEmpty()) {
				Timeline fade = new Timeline(new KeyFrame(Duration.ZERO, 
				        new KeyValue(opacity, 1.0)), new KeyFrame(new Duration(
				                duration), new EventHandler<ActionEvent>() {
							@Override
							public void handle(final ActionEvent t) {
								getChildren().remove(0);
							}
						}, new KeyValue(opacity, 0.0)));
				fade.play();
			}
			return true;
		}
		if (screens.get(name) != null) { // screen loaded

			if (!getChildren().isEmpty() && getChildren().get(0) 
			        != screens.get(name)) {

				Timeline fade = new Timeline(new KeyFrame(Duration.ZERO, 
				        new KeyValue(opacity, 1.0)),
						new KeyFrame(new Duration(150), 
						        new EventHandler<ActionEvent>() {
							@Override
							public void handle(final ActionEvent t) {
							    // remove the displayed screen
								getChildren().remove(0); 
                                // add the screen
								getChildren().add(0, screens.get(name));
								Timeline fadeIn = new Timeline(new KeyFrame(
								        Duration.ZERO, new KeyValue(opacity,
								                0.0)), new KeyFrame(
								                        new Duration(150),
								                        new KeyValue(opacity,
								                                1.0)));
								fadeIn.play();
							}
						}, new KeyValue(opacity, 0.0)));
				fade.play();

			} else if (getChildren().isEmpty()) {
				setOpacity(0.0);
				getChildren().add(screens.get(name)); // no one else been
														// displayed, then just
														// show
				Timeline fadeIn = new Timeline(new KeyFrame(Duration.ZERO, 
				        new KeyValue(opacity, 0.0)), new KeyFrame(new Duration(
				                150), new KeyValue(opacity, 1.0)));
				fadeIn.play();
			}

			setTopAnchor(screens.get(name), 0.0);
			setBottomAnchor(screens.get(name), 0.0);
			setRightAnchor(screens.get(name), 0.0);
			setLeftAnchor(screens.get(name), 0.0);

			return true;
		} else {
			System.out.println("screen hasn't been loaded!!! \n");
			return false;
		}
	}

	// This method will remove the screen with the given name from the
	// collection of screens
	/**
	 * @param name The name of the node
	 * @return true if it is removed
	 */
	public final boolean unloadScreen(final String name) {
		return !(screens.remove(name) == null);
	}
}
